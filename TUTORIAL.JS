//Require needed libraries
const { WebClient } = require('@slack/client');
const { IncomingWebhook } = require('@slack/client');
var testMessage = require('./test-message.json')

//Log message to terminal
console.log('Begin test run of snorri');

//Get the Incoming Webhook url from an environment variable
const url = process.env.SLACK_WEBHOOK_URL;

//Get Slack Authentication  Token from environment variable called "SLACK_TOKEN"
const token = process.env.SLACK_TOKEN;

//Create an instance of the IncomingWebhook class to post messages to the chart with
const webhook = new IncomingWebhook(url);

// Send simple text to the webhook channel
webhook.send(testMessage, function(err, res) {
    if (err) {
        console.log('Error:', err);
    } else {
        console.log('Message sent: ', res);
    }
});