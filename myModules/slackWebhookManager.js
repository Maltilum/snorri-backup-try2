/**
 * Module to clean up main program code by moving slack webhook functionality over here. 
 */


//Require needed libraries
const { IncomingWebhook } = require('@slack/client');

//-------------------------Get Environment Variables-------------------
//Get the Incoming Webhook url from an environment variable
const slackWebhookUrl = process.env.SLACK_WEBHOOK_URL;


//-------------------------Debug Print----------------------------------
//console.log(slackWebhookUrl);

//-------------------------Create Incoming Webhooks Handler
const slackWebHook = new IncomingWebhook(slackWebhookUrl);


//-------------------------Functions----------------------------------
var testSlackSend = function(message)
{
    slackWebHook.send(message, (error, resp)=> {
        if (error) {
          return console.error(error);
        }
    });
};
module.exports.testSlackSend = testSlackSend;

