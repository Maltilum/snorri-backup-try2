//Require needed libraries
const { IncomingWebhook } = require('@slack/client');
const { RTMClient } = require('@slack/client');


//-------------------------Get Environment Variables-------------------
//Get the Incoming Webhook url from an environment variable
const slackWebhookUrl = process.env.SLACK_WEBHOOK_URL;

//Get Slack Authentication  Token from environment variable called "SLACK_TOKEN"
const token = process.env.SLACK_TOKEN;


//-------------------------Debug Print----------------------------------
//console.log(slackWebhookUrl);

//-------------------------Create Incoming Webhooks Handler
const slackWebHook = new IncomingWebhook(slackWebhookUrl);


//-------------------------Functions----------------------------------
var testSlackSend = function(message)
{
    slackWebHook.send(message, (error, resp)=> {
        if (error) {
          return console.error(error);
        }
    });
};
module.exports.testSlackSend = testSlackSend;

