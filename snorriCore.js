//Require needed libraries
const { IncomingWebhook } = require('@slack/client');
const { RTMClient } = require('@slack/client');


//-------------------------Get Environment Variables-------------------
//Get the Incoming Webhook url from an environment variable
const url = process.env.SLACK_WEBHOOK_URL;

//Get Slack Authentication  Token from environment variable called "SLACK_TOKEN"
const token = process.env.SLACK_TOKEN;

//-------------------------Instantiate instances of RTM Client-------------------
