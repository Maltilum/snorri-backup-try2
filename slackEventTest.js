const http = require('http');
const { WebClient } = require('@slack/client');

//Set up WEBAPI, used to call chat.unfurl to unfurl the links in response to a link_shared event
// An access token (from your Slack app or custom integration - xoxp, xoxb, or xoxa)
const token = process.env.SLACK_TOKEN;
const web = new WebClient(token);

//Set up EVENTS_API used to receive link_shared event from slack and respond with a chat.unfurl function from the WEB_API
// Initialize using verification token from environment variables
const createSlackEventAdapter = require('@slack/events-api').createSlackEventAdapter;
const slackEvents = createSlackEventAdapter(process.env.SLACK_VERIFICATION_TOKEN);
const port = process.env.PORT || 3000;

// Initialize an Express application
const express = require('express');
const bodyParser = require('body-parser');
const app = express();

// You must use a body parser for JSON before mounting the adapter
app.use(bodyParser.json());

// Mount the event handler on a route
// NOTE: you must mount to a path that matches the Request URL that was configured earlier
app.use('/slack/events', slackEvents.expressMiddleware());

// Attach listeners to events by Slack Event "type". See: https://api.slack.com/events/message.im
slackEvents.on('message', (event)=> {
  console.log(`Received a message event: user ${event.user} in channel ${event.channel} says ${event.text}`);
});

slackEvents.on('link_shared', (event)=> {
    
  console.log(`Channel: ${event.channel}`);
  console.log(`Timestamp: ${event.message_ts}`);
  console.log(`URl: ${event.links[0]['url']}`);

  var channel = event.channel;
  var timestamp = event.message_ts;
 
  //Build the json data that is sent back to Slack to display as an unfurled link. This is a test set of values designed to test chat.unfurl
  var textData = {};
  textData["text"] = "Testing Snorri";
  var urlKey = event.links[0]['url'];
  var unfurlData = {}; 
  unfurlData[urlKey] = textData;

  var unfurlMessage = {};
  unfurlMessage['channel'] = event.channel;
  unfurlMessage['ts'] = event.message_ts;
  unfurlMessage["unfurls"] = unfurlData;

  console.log(unfurlMessage);

  web.chat.unfurl({channel: channel, ts: timestamp, unfurls: unfurlData}).then((res) => {
    // `res` contains information about the posted message
    console.log('Message sent: ', res.ts);
  })
  .catch(console.error);;

  });

// Handle errors (see `errorCodes` export)
slackEvents.on('error', console.error);

// Start the express application
http.createServer(app).listen(port, () => {
  console.log(`server listening on port ${port}`);
});